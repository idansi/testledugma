import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;
  name;
  price;
  stock:boolean;

  text;
  tempText;
  key;

  showTheButton = false;
  delete = false;
  showButton(){
    this.showTheButton = true;
  }

  hideButton(){
    this.showTheButton = false;
  }

  openDelete(){
    this.delete = true;
  }
  deleteItem(){
    this.itemsService.deleteItem(this.key);
  }

  cancel()
  {
    this.delete = false;
  }

  checkChange()
  {
    this.itemsService.updateItem(this.key,this.stock);
    console.log(this.key, this.stock);
  }

  constructor(private itemsService:ItemsService) { }

  ngOnInit() {

    this.name = this.data.name;
    this.price = this.data.price;
    this.stock = this.data.stock;
    this.key = this.data.$key;

  }
 
}
