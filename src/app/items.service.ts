import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  deleteItem(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/'+user.uid+'/items').remove(key);
    })
  }

  updateItem(key:string, stock:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/'+user.uid+'/items').update(key,{'stock':stock});
    })
  }


  constructor(public authService:AuthService, private db:AngularFireDatabase) { }
}
