import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { ItemsService } from '../items.service';

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items = [];

  constructor(public authService:AuthService, private router:Router, private db:AngularFireDatabase, private itemsService:ItemsService) { }

  ngOnInit() {

    this.authService.user.subscribe(user => {
      this.db.list('/' + user.uid + '/items').snapshotChanges().subscribe(
        items => {
          this.items = [];
          items.forEach(
    
            item => {
              let y = item.payload.toJSON();
              y["$key"] = item.key;
              this.items.push(y);
            }
          ); console.log(this.items);
        }
      )
    })




  }

}
