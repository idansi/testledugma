import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent implements OnInit {

  email='';

  constructor(public authService:AuthService, private db:AngularFireDatabase ) { }

  ngOnInit() {
    
    this.authService.user.subscribe(user=>{
    if(user != null){
    this.email=user.email;
                    }
    })
  }

}
